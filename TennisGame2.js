const SCORE_CERO = "Love";
const SCORE_ONE = "Fifteen";
const SCORE_TWO = "Thirty";
const SCORE_THREE = "Forty";

class TennisGame2 {
    constructor(player1Name, player2Name) {
        this.P1point = 0;
        this.P2point = 0;

        this.player1Name = player1Name;
        this.player2Name = player2Name;
    }

    getScore() {
        let score = "";
        if (this.P2point < 4 && this.P1point < 4){
            score =this.calculateScoresUnder3(this.P1point,this.P2point);
        }
        else
            score =this.calculateScoresAbove3(this.P1point,this.P2point);

        return score;
    }

    calculateScoresUnder3(pointsPlayer1,pointsPlayer2){
        if (pointsPlayer1 === 0 )
            return this.getScoreFromCeroTo(pointsPlayer2);

        if (pointsPlayer1 === 1)
            return this.getScoreFromOneTo(pointsPlayer2);

        if (pointsPlayer1 === 2)
            return this.getScoreFromTwoTo(pointsPlayer2);

        if (pointsPlayer1 === 3)
            return this.getScoreFromThreeTo(pointsPlayer2);
    }

    calculateScoresAbove3(pointsPlayer1, pointsPlayer2) {
        if (pointsPlayer1 === 4 && pointsPlayer2 === 4)
            return "Deuce";

        if ( (pointsPlayer1 - pointsPlayer2) === 1)
            return "Advantage player1";

        if ( (pointsPlayer2 - pointsPlayer1) === 1)
            return "Advantage player2";

        if ( (pointsPlayer1 - pointsPlayer2) >= 2) {
            return "Win for player1";
        }
        if ( (pointsPlayer2 - pointsPlayer1) >= 2) {
            return "Win for player2";
        }
    }

    getScoreFromCeroTo(pointPlayer){
        if (pointPlayer === 0)
            return SCORE_CERO + "-All";
        if (pointPlayer === 1)
            return SCORE_CERO + "-" + SCORE_ONE;
        if (pointPlayer === 2)
            return SCORE_CERO + "-" + SCORE_TWO;
        if (pointPlayer === 3)
            return SCORE_CERO + "-" + SCORE_THREE;
    }

    getScoreFromOneTo(pointPlayer){
        if (pointPlayer === 0)
            return SCORE_ONE + "-"+SCORE_CERO;
        if (pointPlayer === 1)
            return SCORE_ONE + "-All";
        if (pointPlayer === 2)
            return SCORE_ONE + "-" + SCORE_TWO;
        if (pointPlayer === 3)
            return SCORE_ONE + "-" + SCORE_THREE;
    }

    getScoreFromTwoTo(pointPlayer){
        if (pointPlayer === 0)
            return SCORE_TWO + "-"+SCORE_CERO;
        if (pointPlayer === 1)
            return SCORE_TWO + "-"+SCORE_ONE;
        if (pointPlayer === 2)
            return SCORE_TWO + "-All";
        if (pointPlayer === 3)
            return SCORE_TWO + "-" + SCORE_THREE;
    }

    getScoreFromThreeTo(pointPlayer){
        if (pointPlayer === 0)
            return SCORE_THREE + "-"+SCORE_CERO;
        if (pointPlayer === 1)
            return SCORE_THREE + "-"+SCORE_ONE;
        if (pointPlayer === 2)
            return SCORE_THREE + "-"+SCORE_TWO;
        if (pointPlayer === 3)
            return "Deuce";
    }


    SetP1Score(number) {
        let i;
        for (i = 0; i < number; i++) {
            this.P1Score();
        }
    }

    SetP2Score(number) {
        let i;
        for (i = 0; i < number; i++) {
            this.P2Score();
        }
    }

    P1Score() {
        this.P1point++;
    }

    P2Score() {
        this.P2point++;
    }

    wonPoint(player) {
        if (player === "player1")
            this.P1Score();
        else
            this.P2Score();
    }



}

if (typeof window === "undefined") {
    module.exports = TennisGame2;
}
